<?php
/**
 * Created by PhpStorm.
 * User: renan
 * Date: 07/09/15
 * Time: 19:17
 */

class Config {

    private $host = 'localhost';
    private $dbname = '';
    private $user = 'root';
    private $pass = '';

    public function conn(){

        $conn = new PDO(
            'mysql:host='.$this->host.';dbname='.$this->dbname,
            $this->user,
            $this->pass,
            array(
                PDO::MYSQL_ATTR_INIT_COMMAND=>'SET NAMES utf8'
            )
        );

        return $conn;
    }

    public function getExp($ids=null, $lg){
      if (empty($ids)) {
        $select = $this->conn()->prepare("SELECT id,$lg as lg FROM `exp` WHERE 1");
      } else {
        $exps = implode(',', $ids);
        $select = $this->conn()->prepare("SELECT id,$lg as lg FROM `exp` WHERE `id` IN($exps)");
      }
      $select->execute();
      $expressions = array_map('reset', $select->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC));
      $exp = array_combine(
          array_map(function($key){ return 'exp_'.$key; }, array_keys($expressions)),
          $expressions
      );
      return $exp;

    }

    public function getMenu($lg){

      $select = $this->conn()->prepare("SELECT m.id, e.$lg as title, m.slug FROM menu m INNER JOIN exp e ON m.name = e.id WHERE `ativo` = 1");
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);

    }

    // public function getMegaMenu($lg){
    //
    //   $select = $this->conn()->prepare("SELECT m.id, e.$lg as title, m.slug FROM menu m INNER JOIN exp e ON m.name = e.id WHERE `ativo` = 1");
    //   $select->execute();
    //   $menu = $select->fetchAll(PDO::FETCH_ASSOC);
    //
    //   $select2 = $this->conn()->prepare("SELECT c.nome as categoria, m.nome as marca, a.nome as area FROM produtos_cat c
    //                                     INNER JOIN produtos p ON p.categoria = c.id
    //                                     INNER JOIN marcas m ON p.id_marca = m.id
    //                                     INNER JOIN areas a ON m.id_area = a.id
    //                                     -- WHERE a.id = 1
    //                                     GROUP BY c.id ORDER BY m.id ASC");
    //   $select2->execute();
    //   $categorias = $select2->fetchAll(PDO::FETCH_ASSOC);
    //
    //   $mega_menu = array();
    //   // foreach ($categorias as $key => $value) {
    //   //   if(array_key_exists($value['area'], $mega_menu)){
    //   //     // if(array_key_exists($value['marca'], $mega_menu[$value['area']])){
    //   //     if (!in_array($value['marca'], $mega_menu[$value['area']])){
    //   //       if(array_key_exists($value['marca'], $mega_menu[$value['area']])){
    //   //         if (!in_array($value['categoria'], $mega_menu[$value['area']][$value['marca']])){
    //   //           array_push($mega_menu[$value['area']][$value['marca']], $value['categoria']);
    //   //         } else {
    //   //           continue;
    //   //         }
    //   //       } else {
    //   //         $mega_menu[$value['area']][$value['marca']] = array($value['categoria']);
    //   //       }
    //   //     } else {
    //   //       continue;
    //   //     }
    //   //   } else {
    //   //     $mega_menu[$value['area']] = array($value['marca'] => array());
    //   //   }
    //   // }
    //   foreach ($categorias as $key => $value) {
    //     if(array_key_exists($value['marca'], $mega_menu)){
    //       array_push($mega_menu[$value['marca']], $value['categoria']);
    //     } else {
    //       $mega_menu[$value['marca']] = array($value['categoria']);
    //     }
    //   }
    //
    //   //ordenando array do maior array para o menor
    //   array_multisort(array_map('count', $mega_menu), SORT_DESC, $mega_menu);
    //
    //   foreach ($menu as $key => $value) {
    //     if ($menu[$key]['slug'] == 'produtos') {
    //       $menu[$key]['mega'] = $mega_menu;
    //     }
    //   }
    //
    //   return $menu;
    //
    // }

    public function getMegaMenu($lg){

      $select = $this->conn()->prepare("SELECT m.id, e.$lg as title, m.slug FROM menu m INNER JOIN exp e ON m.name = e.id WHERE `ativo` = 1");
      $select->execute();
      $menu = $select->fetchAll(PDO::FETCH_ASSOC);

      $select2 = $this->conn()->prepare("SELECT c.nome as categoria, m.nome as marca, a.nome as area FROM produtos_cat c
                                        INNER JOIN produtos p ON p.categoria = c.id
                                        INNER JOIN marcas m ON p.id_marca = m.id
                                        INNER JOIN areas a ON m.id_area = a.id
                                        GROUP BY c.id, m.id ORDER BY m.ordem ASC, c.id ASC");
      $select2->execute();
      $categorias = $select2->fetchAll(PDO::FETCH_ASSOC);

      $mega_menu = array();
      foreach ($categorias as $key => $value) {
        if(array_key_exists($value['area'], $mega_menu)){
          // if(array_key_exists($value['marca'], $mega_menu[$value['area']])){
          if (!in_array($value['marca'], $mega_menu[$value['area']])){
            if(array_key_exists($value['marca'], $mega_menu[$value['area']])){
              if (!in_array($value['categoria'], $mega_menu[$value['area']][$value['marca']])){
                array_push($mega_menu[$value['area']][$value['marca']], $value['categoria']);
              } else {
                continue;
              }
            } else {
              $mega_menu[$value['area']][$value['marca']] = array($value['categoria']);
            }
          } else {
            continue;
          }
        } else {
          $mega_menu[$value['area']] = array($value['marca'] => array());
        }
      }

      //ordenando array do maior array para o menor
      // array_multisort(array_map('count', $mega_menu), SORT_DESC, $mega_menu);

      $mega_menu_final = array();
      foreach ($menu as $key => $value) {
        if ($menu[$key]['slug'] == 'produtos') {
          // foreach ($mega_menu as $key2 => $value2) {
          //   array_multisort(array_map('count', $mega_menu[$key2]), SORT_DESC, $mega_menu[$key2]);
          // }
          $menu[$key]['mega'] = $mega_menu;
        }
      }

      return $menu;

    }

}
