<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

$loader->get('src/Model/Mail');

$mensagem_erro = false;

if (Request::isAjax()){

  Resources::ajaxProtect($_POST, $_SESSION);

  if($action == 'send'){

    for ($c = 0; $c < count($_POST['dados']); $c++) {
        if($_POST['dados'][$c]['value'] == ''){
            $falha = true;
            $message[$c] = ucfirst($_POST['dados'][$c]['name']).' está vazio';
        } else {
            $dados[$c] = $_POST['dados'][$c]['value'];
        }
    }

    // [0] => Nome
    // [1] => email
    // [2] => assunto
    // [3] => mensagem

    // echo '<pre>';
    // print_r($dados);
    // echo '</pre>';
    // exit();

    //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    $html = '<h1>Contacto do Site Eurotenologia</h1>';
    $html .= '<p>Nome: '.$dados[0].'</p>';
    $html .= '<p>Email: '.$dados[1].'</p>';
    $html .= '<p>Assunto: '.$dados[2].'</p>';
    $html .= '<p>Mensagem: '.$dados[3].'</p>';

    if (SendMail::trySendMail('Contacto Eurotenologia', 'info@eurotecnologia.pt', $html, new PHPMailer(true))) {
      echo json_encode(array('1', '<strong>Sucesso!</strong> Mensagem enviada com sucesso.'));
      exit();
    } else {
      echo json_encode(array('0', '<strong>Erro!</strong> O sistema não conseguiu enviar o e-mail.'));
      exit();
    }
    //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------


  }

}

  // echo '<pre>';
  // print_r(array_column($catalogos, 'nome'));
  // echo '</pre>';

?>
