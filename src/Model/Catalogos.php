<?php

class Catalogos{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function getCatalogoLink($cid){
      $select = $this->mysql->prepare('SELECT id, catalogo FROM `marcas` WHERE id = :id ');
      $select->bindValue(':id', $cid, PDO::PARAM_STR);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getCatalogos(){
      $select = $this->mysql->prepare('SELECT * FROM `marcas` WHERE `catalogo` IS NOT NULL');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    //CRUD

    public function insertCat($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO produtos_cat (nome, slug, ativo) VALUES (:nome, :slug, 1);');
            $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
            $cadastra->bindValue(':slug', Resources::slug($dados['nome']), PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readCat($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCat($dados){
        $deletef = $this->mysql->prepare('UPDATE produtos_cat SET nome = :nome WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM produtos_cat WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
